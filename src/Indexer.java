/**
 * Created by zhangmingke on 16/5/16.
 */


import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.jcp.xml.dsig.internal.dom.Utils;

import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.Date;
import Utils.DBUtils;

public class Indexer {
    private Indexer() {
    }

    public static final String indexPath = "src/index";
    public static ResultSet resultSet = null;
    public static Statement statement = null;
    public static Connection connect = null;

    public static void main(String[] args) {

        String query = null;
        String connection = null;
        boolean create = true;

        Date start = new Date();
        try {
            System.out.println("Indexing to directory '" + indexPath + "'...");

            Directory dir = FSDirectory.open(new File(indexPath).toPath());
            Analyzer analyzer = new StandardAnalyzer();
            IndexWriterConfig iwc = new IndexWriterConfig(analyzer);

            if (create) {
                // Create a new index in the directory, removing any
                // previously indexed documents:
                iwc.setOpenMode(OpenMode.CREATE);
            } else {
                // Add new documents to an existing index:
                iwc.setOpenMode(OpenMode.CREATE_OR_APPEND);
            }

            IndexWriter writer = new IndexWriter(dir, iwc);
            DBUtils.indexDocs(resultSet, statement, connect, writer, query, connection);
            writer.close();

            Date end = new Date();
            System.out.println(end.getTime() - start.getTime() + " total milliseconds");

        } catch (IOException e) {
            System.out.println(" caught a " + e.getClass() + "\n with message: " + e.getMessage());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
