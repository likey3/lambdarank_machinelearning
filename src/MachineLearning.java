import Models.TrainData;
import Utils.DBUtils;
import ciir.umass.edu.learning.Evaluator;
import ciir.umass.edu.learning.RANKER_TYPE;
import ciir.umass.edu.utilities.MyThreadPool;
import org.jcp.xml.dsig.internal.dom.Utils;
import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import Utils.LogUtils;

/**
 * Created by liuchuliang on 16/5/16.
 */
public class MachineLearning {
    public static void train(List<TrainData> trainData) {
        LogUtils.log("Program is training data ...", LogUtils.LOG_LEVEL.DEBUG);
        String trainFile = "train_set/train.txt";

        if (trainData != null) TrainData.writeToTrainFile(trainData);

        MyThreadPool.init(Runtime.getRuntime().availableProcessors());

        Evaluator.modelFile = "train_set/model.txt";
        Evaluator e = new Evaluator(RANKER_TYPE.LAMBDARANK, "NDCG@10", "ERR@10");
        e.evaluate(trainFile, 0.1, "", "");

        MyThreadPool.getInstance().shutdown();
    }

    public static List<Integer> getScoreList(List<TrainData> trainData) {
        LogUtils.log("Program is geting ScoreList ...", LogUtils.LOG_LEVEL.DEBUG);
        String modelFile = "train_set/model.txt";
        String testFile = "train_set/test.txt";
        String rankFile = "train_set/rank.txt";
        String scoreFile = "train_set/score.txt";

        if (trainData != null) TrainData.writeToTestSet(trainData);

        Evaluator e = new Evaluator(RANKER_TYPE.LAMBDAMART, "NDCG@5", "ERR@5");
        Evaluator.modelFile = "train_set/model.txt";
        MyThreadPool.init(Runtime.getRuntime().availableProcessors());
        List<Integer> result = e.rank(modelFile, testFile);
        //e.score(modelFile, testFile, scoreFile);
        MyThreadPool.getInstance().shutdown();

        return result;
    }



    public static void evaluatingModel(String measure) {
        LogUtils.log("Program is evaluating Model ...", LogUtils.LOG_LEVEL.DEBUG);
        String modelFile = "train_set/model.txt";
        String testFile = "train_set/eval.txt";

        MyThreadPool.init(Runtime.getRuntime().availableProcessors());

        Evaluator e = new Evaluator(RANKER_TYPE.LAMBDAMART, measure, measure);
        e.test(modelFile, testFile);

        MyThreadPool.getInstance().shutdown();
    }

    public static List<Double> transform(List<Double> list) {
        int n = list.size() - 1;

        list.forEach(x -> {
            int count = 0;
            for (Double d : list) if (d <= x) count++;

            System.out.println(1.0 * count / n);
        });
        return list;
    }
}
