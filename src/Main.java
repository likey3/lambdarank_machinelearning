import ciir.umass.edu.learning.Evaluator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by liuchuliang on 16/5/16.
 */
public class Main {
    public static void main(String[] args) {
        //String[] params = {"", ""};
        //Evaluator.main(params);
        MachineLearning.train(null);

        MachineLearning.evaluatingModel("NDCG@10");
        // MachineLearning.getScoreList(null);

        List<Double> list = new ArrayList<Double>();

        list.add(1.2);
        list.add(1.2);
        list.add(1.2);
        list.add(1.2);
        list.add(1.2);
        list.add(1.2);

        MachineLearning.transform(list).forEach(x -> {
            //System.out.println(x);
        });
    }
}
