package Models;

import Utils.LogUtils;

import java.util.Collection;
import java.util.List;

/**
 * Created by liuchuliang on 16/5/16.
 */
public class QueryLog {
    public int doc_id, count;
    public long time;
    public String queryString;

    public QueryLog(int doc_id, int count, long time, String queryString) {
        this.doc_id = doc_id;
        this.count = count;
        this.time = time;
        this.queryString = queryString;
    }

    // set the doc_id and queryString as the common primary
    public String hashcode() {
        return getQueryHash(doc_id, queryString);
    }

    public static String hashcode(int doc_id, String queryString) {
        return doc_id + Integer.toString(queryString.hashCode());
    }

    public String toString() {
        return String.format("%d %s %d %d", doc_id, queryString, count, count, time);
    }

    public static void printQueryLog(Collection<QueryLog> queryLogList) {
        if (queryLogList.size() == 0)
            LogUtils.log("query log empty", LogUtils.LOG_LEVEL.RELEASE);

        queryLogList.forEach(x -> {
            LogUtils.log(x.toString(), LogUtils.LOG_LEVEL.RELEASE);
        });
    }

    public static String getQueryHash(int doc_id, String query_word) {
        return  doc_id + Integer.toString(query_word.hashCode());
    }
}


