package Models;

import Utils.LogUtils;
import kmeans.KMeans;
import kmeans.LocationWrapper;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by liuchuliang on 16/5/16.
 */
public class TrainData {
    public int qid, counts, times, score = 0;
    public double similarity;
    public double norm_sim, norm_count, norm_time;

    public TrainData(int qid, int counts, int times, int score, double similarity) {
        this.qid = qid;
        this.counts = counts;
        this.times = times;
        this.score = score;
        this.similarity = similarity;
    }

    @Override
    public String toString() {
        return String.format("qid:%d counts:%d times:%d score:%d similarity:%g",
                qid, counts, times, score, similarity);
    }

    public String toTrainDataString() {
        return String.format("%d qid:%d 1:%g 2:%d 3:%d",
                score, qid, similarity, counts, times);
    }

    public static void calScore(List<TrainData> trainDataList) {
        List<LocationWrapper> list = new ArrayList<>(trainDataList.size());
        List<TrainData> clusterData = new ArrayList<>(trainDataList.size());

        int index = 0;
        for (TrainData data : trainDataList) {
            if (data.counts == 0 && data.times == 0) {
                data.score = 0;
            } else {
                clusterData.add(data);
                list.add(new LocationWrapper(data.counts, data.times, index++));
            }
        }

        int[] scoreList = KMeans.getSortedLabel(list);

        for (int i = 0; i < clusterData.size(); i++) {
            clusterData.get(i).score = scoreList[i];
        }
    }

    public static void writeToTrainFile(List<TrainData> trainSet) {
        try {
            StringBuilder sb = new StringBuilder(1000);
            trainSet = normalizedTrainSet(trainSet);

            for (TrainData data : trainSet) {
                sb.append(String.format("%d qid:%d 1:%6f 2:%6f 3:%6f",
                        data.score, data.qid, data.norm_sim
                        , data.norm_count, data.norm_time));
                sb.append(System.lineSeparator());
            }

            FileUtils.write(new File("train_set/train.txt"), sb.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<TrainData> normalizedTrainSet(List<TrainData> trainSet) {
        int n = trainSet.size() - 1;

        for (int i = 0, size = trainSet.size(); i < size; i++) {
            TrainData x = trainSet.get(i);
            int count1 = 0, count2 = 0, count3 = 0;

            for (TrainData d : trainSet) {
                if (d.similarity < x.similarity) count1++;
                if (d.counts < x.counts) count2++;
                if (d.times < x.times) count3++;

            }

            x.norm_sim = 1.0 * count1 / n;
            x.norm_count = 1.0 * count2 / n;
            x.norm_time = 1.0 * count3 / n;
        }

        return trainSet;
    }

    public static void writeToTestSet(List<TrainData> testSet) {
        try {
            StringBuilder sb = new StringBuilder(1000);

            for (TrainData data : testSet) {
                sb.append(String.format(Locale.ENGLISH, "%d qid:%d 1:%6f 2:%6f 3:%6f",
                        0, data.qid, data.norm_sim
                        , data.norm_count, data.norm_time));
                sb.append(System.lineSeparator());
            }

            FileUtils.write(new File("train_set/test.txt"), sb.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
