
import Models.QueryLog;
import Models.SQLModel;
import Models.TrainData;
import Utils.DBUtils;
import Utils.LogUtils;
import org.apache.commons.math3.analysis.function.Log;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.*;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.InvalidTokenOffsetsException;
import org.apache.lucene.search.highlight.QueryScorer;
import org.apache.lucene.search.highlight.SimpleFragmenter;
import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.store.FSDirectory;

import javax.print.Doc;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.*;
import java.util.*;


public class Searcher {

    Searcher() {
    }
    static Connection connect = null;
    static Statement statement = null;
    static ResultSet resultSet = null;

    static HashMap<String, TrainData> hash = null;
    static Random random = new Random(System.currentTimeMillis());
    static int qid = random.nextInt(1000);
    static List<TrainData> trainSet = new ArrayList<>(1000);
    static HashMap<String, QueryLog> queryLog = new HashMap<>(1000);

    public static void main(String[] args) throws Exception {
        // String query

        // storage location of the index
        String index = "src/index";

        // columns of Metadata_FullText table having been INDEXED ny Lucene
        // change this okay
        String[] fields = { "FTIDS", "IDS", " CLASSIDS", "TEXTS", "PUBNOTICECONTENTS", "DOCUMENTCONTENTS",
                "contentSum_DES" };


        String queries = null;
        boolean raw = false;
        String queryString = null;
        int hitsPerPage = 10;

        // Reading data from the index
        IndexReader reader = DirectoryReader.open(FSDirectory.open(new File(index).toPath()));
        // performing search over read data
        IndexSearcher searcher = new IndexSearcher(reader);

        float k1 = (float) 1.2;
        float b = (float) 0.75;

        init();
        // using BM25 Ranking Model over searched data to rank
        // k - Controls non-linear term frequency normalization saturation.
        // b - Controls to what degree document length normalizes tf values.
        // k = 1.2, b=0.75
        // similarity for multiple fields[]
        searcher.setSimilarity(new BM25Similarity(k1, b));
        // multiple fields query parser
        //Map<String , Float> boosts = null;
        MultiFieldQueryParser parser = new MultiFieldQueryParser(fields, new StandardAnalyzer());
        // Single field query parser
        // QueryParser parser = new QueryParser(fields, analyzer);

        boolean endProgram = false;

        while (!endProgram) {
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in, "UTF-8"));
            if (queries == null && queryString == null) { // prompt the user
                LogUtils.log("Enter query: ", LogUtils.LOG_LEVEL.RELEASE);
            }

            String line = queryString != null ? queryString : in.readLine();
            //String line = "china";
            if (line == null || line.length() == -1) {
                break;
            }

            line = line.trim();
            if (line.length() == 0) {
                break;
            }

            hash = new HashMap<>(100);
            qid++;
            Query query = parser.parse(line);
            LogUtils.log("Searching for: " + query.toString() , LogUtils.LOG_LEVEL.RELEASE);
            String read = line.toString();
            //dataStore(read, hitsPerPage, fields, null, 0, null);
            endProgram = doPagingSearch(in, reader, searcher, query, hitsPerPage, raw, queries == null && queryString == null,
                    fields, null, read, 0);
        }

        closing();
        LogUtils.log("    !!!The program end!!!   ", LogUtils.LOG_LEVEL.RELEASE);
        reader.close();
    }

    private static void closing() {
        LogUtils.log("Program is closing...\n", LogUtils.LOG_LEVEL.DEBUG);
        DBUtils.writeDataSet(hash.values());
        DBUtils.savaToQueryLogDatabase(queryLog);

        LogUtils.log("Program starts to train ...\n", LogUtils.LOG_LEVEL.DEBUG);
        MachineLearning.train(trainSet);

        MachineLearning.evaluatingModel("NDCG@5");
        MachineLearning.evaluatingModel("NDCG@10");
    }

    private static void init() throws Exception {
        DBUtils.initTrainSet(trainSet);
        DBUtils.initQueryLog(queryLog);

        // QueryLog.printQueryLog(queryLog.values());
        // init qid
        if (trainSet.size() <= 0) {
            qid = 0;
        } else {
            qid = trainSet.stream().mapToInt(x -> x.qid).max().getAsInt() + 1;
        }
    }


    public static boolean doPagingSearch(BufferedReader in, IndexReader reader, IndexSearcher searcher, Query query,
                                         int hitsPerPage, boolean raw, boolean interactive, String[] fields, Document doc, String read,
                                         float newScore) throws IOException, ClassNotFoundException, SQLException, InvalidTokenOffsetsException {
        // Collect enough docs to show 5 pages
        TopDocs results = searcher.search(query, 5 * hitsPerPage);

        BoldFormatter formatter = new BoldFormatter();

        Highlighter highlighter = new Highlighter(formatter, new QueryScorer(query));
        highlighter.setTextFragmenter(new SimpleFragmenter(50));

        //ScoreDoc[] hits = results.scoreDocs;

        ScoreDoc[] hits = reRankHitDocs(results.scoreDocs, searcher, read);
        int numTotalHits = results.totalHits;

        LogUtils.log(numTotalHits + " total matching documents \n", LogUtils.LOG_LEVEL.RELEASE);

        int start = 0;

        int end = Math.min(numTotalHits, hitsPerPage);
        boolean endSearchFun = false;
        while (!endSearchFun) {
            if (end > hits.length) {
                LogUtils.log("Only results 1 - " + hits.length + " of " + numTotalHits
                        + " total matching documents collected.", LogUtils.LOG_LEVEL.RELEASE);
                LogUtils.log("Collect more (y/n) ?", LogUtils.LOG_LEVEL.RELEASE);
                String line = in.readLine();
                if (line.length() == 0 || line.charAt(0) == 'n') {
                    break;
                }
                results = searcher.search(query, numTotalHits);
                hits = reRankHitDocs(results.scoreDocs, searcher, read);
            }

            end = Math.min(hits.length, start + hitsPerPage);
            for (int i = start; i < end; i++) {
                doc = searcher.doc(hits[i].doc);

                printDocument(doc, hits, fields, reader, highlighter, i, raw, read, 0);
                //			dataStore(read, 0, null, null, 0, null);
            }

            if (!interactive || end == 0) {
                break;
            }
            // hits per page(10) n--next piage, p--previous page, q--quit,
            // c--choose
            if (numTotalHits >= end) {
                boolean quit = false;
                while (!quit) {
                    System.out.print("Press ");

                    if (start - hitsPerPage >= 0) {
                        System.out.print("(p)revious page, ");
                    }
                    if (start + hitsPerPage < numTotalHits) {
                        System.out.print("(n)ext page, ");
                    }

                    System.out.print("(c)hoose a Document. (q)uit current query. (e)nd the program \n");

                    // reading input from user to make an action of n,p,q,c
                    String line = in.readLine();

                    if (line.charAt(0) == 'c') {

                        int choose = Integer.parseInt(in.readLine());

                        long startTime = System.currentTimeMillis();

                        LogUtils.log("document selected: " + choose, LogUtils.LOG_LEVEL.RELEASE);

                        // start = choose;
                        if(choose>=start && choose<=end) {
                            choose--; // change the index
                        }

                        printDocument(searcher.doc(hits[choose].doc), hits, fields, reader, highlighter, choose, raw,
                                read, 0);
                        doc = searcher.doc(hits[choose].doc);

                        String ID = doc.get("FTIDS");
                        LogUtils.log("press (b)ack or press (o)ut", LogUtils.LOG_LEVEL.RELEASE);

                        BufferedReader br = new BufferedReader(new InputStreamReader(System.in, "UTF-8"));
                        String out = br.readLine();

                        if (out.charAt(0) == 'o' || out.charAt(0) == 'b') {
                            long endTime = System.currentTimeMillis();
                            long totalTime = endTime - startTime;
                            LogUtils.log("Total time" + totalTime, LogUtils.LOG_LEVEL.RELEASE);
                            // After each query ( selected with the document ) , save a query to a database record
                            // DBUtils.dataStore(read, hitsPerPage, fields, ID, totalTime, null);

                            int score = -1;

                            do {
                                LogUtils.log("please give this document a score(input 0 or 1):", LogUtils.LOG_LEVEL.RELEASE);

                                try {
                                    score = Integer.parseInt(in.readLine());

                                    if (score == 0 || score == 1) {
                                        hash.get(ID).score = score;
                                    }
                                } catch (NumberFormatException e) {
                                    LogUtils.log("input format error! try again please!", LogUtils.LOG_LEVEL.RELEASE);
                                }
                            } while (score != 0 && score != 1);

                            updateTrainData(ID, totalTime);
                            updateQueryLog(queryLog, ID, totalTime, read);

                            if (out.charAt(0) == 'o') quit = true;
                            break;
                        }
                    }

                    if (line.length() == 0 || line.charAt(0) == 'q') {
                        quit = true;
                        endSearchFun = false;
                    }
                    else if (line.charAt(0) == 'p') {
                        start = Math.max(0, start - hitsPerPage);
                        break;
                    } else if (line.charAt(0) == 'n') {
                        if (start + hitsPerPage < numTotalHits) {
                            start += hitsPerPage;
                        }
                        break;
                    } else if (line.charAt(0) == 'e') {
                        quit = true;
                        endSearchFun = true;
                    }
                }
                if (quit) {
                    saveTrainData();
                    break;
                }
                end = Math.min(numTotalHits, start + hitsPerPage);
            }
        }
        return endSearchFun;
    }

    // update HashMap<String, QueryLog> queryLog after each query
    public static void updateQueryLog(HashMap<String, QueryLog> queryLog, String DocID, long oneTotaltime, String read) {
        QueryLog oneQueryLog = null;
        // has change the reason
        String queryLogKey = QueryLog.hashcode(Integer.parseInt(DocID), read.trim());
        if(queryLog.containsKey(queryLogKey)) {
            oneQueryLog = queryLog.get(queryLogKey);
            oneQueryLog.count++;
            oneQueryLog.time += oneTotaltime;
        }else {
            // if the doc be choosed firstly
            oneQueryLog = new QueryLog(Integer.parseInt(DocID), 0, 0, read.trim());
            oneQueryLog.count = 1;
            oneQueryLog.time = oneTotaltime;
        }
        queryLog.put(queryLogKey, oneQueryLog);
        LogUtils.log("Updting QueryLog ...", LogUtils.LOG_LEVEL.DEBUG);
    }

    /*
    1.query query_log get total count, times
    2.get score
    3.generate list
    4.add to trainset and wirte to database
     */
    private static void saveTrainData() {
        LogUtils.log("Program is saving Train Data ..." , LogUtils.LOG_LEVEL.DEBUG);

        /* not need auto scored again*/
        // List<TrainData> newTrainSet = new ArrayList<>(hash.values());
        //TrainData.calScore(newTrainSet);

        for (String doc_id : hash.keySet()) {
            TrainData data = hash.get(doc_id);
            QueryLog logData = queryLog.get(doc_id);

            LogUtils.log(data.toTrainDataString(), LogUtils.LOG_LEVEL.DEBUG);

            if (logData != null) {
                data.counts += logData.count;
                data.times += logData.time;
            }
        }

        // QueryLog.printQueryLog(queryLog.values());
        trainSet.addAll(hash.values());
        LogUtils.log("Saved Train Data" , LogUtils.LOG_LEVEL.DEBUG);
    }

    private static void updateTrainData(String id, long totalTime) {
        TrainData trainData = hash.get(id);
        if (trainData != null) {
            trainData.counts++;
            trainData.times += totalTime;
        } else {
            LogUtils.log("connot found doc id:" + id, LogUtils.LOG_LEVEL.DEBUG);
        }
    }

    private static void printDocument(Document doc, ScoreDoc[] hits, String[] fields, IndexReader reader,
                                      Highlighter highlighter, int i, boolean raw, String read, float newScore)
            throws IOException, InvalidTokenOffsetsException, ClassNotFoundException, SQLException {

		/*
		 *
		 * if (raw) { // output raw format int docID = (int) hits[i].score; for
		 * (int z = 0; z < fields.length; z++) { String text =
		 * doc.get(fields[z]); int maxNumFragmentsRequired = 5; String
		 * fragmentSeparator = "..."; TermPositionVector tpv =
		 * (TermPositionVector) reader.getTermVector(docID, fields[z]);
		 * TokenStream tokenStream = TokenSources.getTokenStream((Terms) tpv);
		 * String result = highlighter.getBestFragments(tokenStream, text,
		 * maxNumFragmentsRequired,fragmentSeparator); System.out.println("doc="
		 * + result + " score=" + hits[i].score);
		 *
		 * continue; } }
		 */

        String FTID = doc.get("FTIDS");
        if (FTID != null) {
            System.out.println((i + 1) + ". " + "FTID: " + FTID);

            // retrieve information from fields of Index w.r.t search
            // keyword

            System.out.println("  Score of the Document:" + hits[i].score);
            System.out.println("  ID:"+ doc.get("IDS"));
            System.out.println("  CLASSID: " + doc.get("CLASSIDS"));
            System.out.println("  TEXT: " + doc.get("TEXTS"));
            System.out.println("  PUBNOTICECONTENT: " + doc.get("PUBNOTICECONTENTS"));
            System.out.println("  DOCUMENTCONTENT: " + doc.get("DOCUMENTCONTENTS"));
            System.out.println("  ContentSum_DE: " + doc.get("contentSum_DES"));
            TrainData trainData = new TrainData(qid, 0, 0, 0, hits[i].score);
            if (!hash.containsKey(FTID)) hash.put(FTID, trainData);
        } else {
            System.out.println((i) + ". " + "Not found in documents");
        }
    }
    //type now  ok i just want confirm it is FTIDS or FTID. I think we have saved in INDEX as FTIDS
//we need to save in reference to FTID not ID
    public static ScoreDoc[] reRankHitDocs(ScoreDoc[] hits, IndexSearcher searcher, String query_word) {
        List<TrainData> trainDataList = new ArrayList<>(hits.length);
        ScoreDoc[] result = new ScoreDoc[hits.length];
        try {
            for (ScoreDoc scoreDoc : hits) {
                Document doc = searcher.doc(scoreDoc.doc);
                String FTID = doc.get("FTIDS");
                TrainData data = null;
                QueryLog log = queryLog.get(QueryLog.getQueryHash(Integer.parseInt(FTID), query_word));

                if (log != null) {
                    data = new TrainData(1, log.count, (int)log.time, 0, scoreDoc.score);
                } else {
                    data = new TrainData(1, 0, 0, 0, scoreDoc.score);
                }
                trainDataList.add(data);
            }

            trainDataList = TrainData.normalizedTrainSet(trainDataList);
            List<Integer> rankList = MachineLearning.getScoreList(trainDataList);

            for (int i = 0; i < hits.length; i++) {
                result[i] = hits[rankList.get(i) - 1];
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }
}
