package Utils;

import Models.QueryLog;
import Models.SQLModel;
import Models.TrainData;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;

import java.io.IOException;
import java.sql.*;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;


/**
 * Created by andy on 16/5/17.
 */
public class DBUtils {
    public static String url = SQLModel.url;
    public static String name = SQLModel.name;
    public static String user = SQLModel.user;
    public static String password = SQLModel.password;

    public static void initTrainSet(List<TrainData> trainSet) {
        Connection connect = null;
        ResultSet rs = null;
        Statement st = null;

        try {
            Class.forName(SQLModel.name);
            connect = DriverManager.getConnection(SQLModel.url,SQLModel.user,SQLModel.password);
            String key = String.format("SELECT id, qid, counts, times, score, similarity FROM TrainData");
            st = connect.createStatement();
            rs = st.executeQuery(key);
            LogUtils.log("Initializing TrainSet ...", LogUtils.LOG_LEVEL.DEBUG);
            while(rs.next()) {
                int id = rs.getInt("id");
                int qid = rs.getInt("qid");
                int counts = rs.getInt("counts");
                int times = rs.getInt("times");
                int score = rs.getInt("score");
                float similarity = rs.getFloat("similarity");
                TrainData train = new TrainData(qid, counts, times, score, similarity);
                trainSet.add(train);
            }

        } catch (Exception e) {
            System.out.println(e);
        } finally {
            try {
                connect.close();
            } catch (Exception e) {
                System.out.println(e);
            }
        }
    }

    public static void initQueryLog(HashMap<String, QueryLog> queryLog ) {
        Connection connect = null;
        ResultSet rs = null;
        Statement st = null;

        try {
            Class.forName(SQLModel.name);
            connect = DriverManager.getConnection(SQLModel.url, SQLModel.user, SQLModel.password);
            String key = String.format("SELECT Doc_ID, Query_String, Doc_Count, totalTime FROM QUERY_LOG");
            st = connect.createStatement();
            rs = st.executeQuery(key);
            LogUtils.log("Initializing QueryLog ...", LogUtils.LOG_LEVEL.DEBUG);
            while(rs.next()) {
                int ID = rs.getInt("Doc_ID");
                String Query_String = rs.getString("Query_String");
                int Doc_Count = rs.getInt("Doc_Count");
                long totalTime = rs.getLong("totalTime");
                QueryLog query = new QueryLog(ID, Doc_Count, totalTime, Query_String);
                queryLog.put(query.hashcode() , query);
            }

        } catch (Exception e) {
            System.out.println(e);
        } finally {
            try {
                connect.close();
            } catch (Exception e) {
                System.out.println(e);
            }
        }
    }

    public static void writeDataSet(Collection<TrainData> values) {
        Connection connect = null;
        Statement st = null;
        try {
            Class.forName(SQLModel.name);
            connect = DriverManager.getConnection(SQLModel.url,SQLModel.user,SQLModel.password);

            for(TrainData data : values) {
                String key = String.format("INSERT INTO  TrainData(qid ,counts ,times ,score,similarity) " +
                                "VALUES(%d, %d, %d, %d, %g)"
                        , data.qid, data.counts, data.times, data.score, data.similarity);
                st = connect.createStatement();
                st.execute(key);
                LogUtils.log("WriteDataSet to Database! ", LogUtils.LOG_LEVEL.DEBUG);
            }

        } catch (Exception e) {
            System.out.println(e);
        } finally {
            try {
                connect.close();
            }
            catch(Exception e) {
                System.out.println(e);
            }
        }
    }
    public static void indexDocs(ResultSet resultSet,  Statement statement, Connection connect, IndexWriter writer, String query, String connection) throws IOException, SQLException {
        try {

            // System.out.println("Query: " + query);
            // System.out.println("Connection: " + connection);
            Class.forName(SQLModel.name);
            // Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            // Setup the connection with the DB
            //connect=DriverManager.getConnection("jdbc:sqlserver://SRAONE-PC\\SQLEXPRESS:1433;" + "database=FullTextDB;"
            //       + "user=amit;" + "password=amit12345");

            // Statements allow to issue SQL queries to the database
            connect = DriverManager.getConnection(SQLModel.url,SQLModel.user,SQLModel.password);
            statement = connect.createStatement();
            // Result set get the result of the SQL query
            // resultSet = statement.executeQuery(
            //        "select ID, NAME, SEX, ADDRESS, POSTCODE, COUNTRY from PERSON_DATA");

            resultSet = statement.executeQuery(
                    "SELECT CAST(FTID as INT) AS FTID, ID, CLASSID, TEXT, PUBNOTICECONTENT,DOCUMENTCONTENT,contentSum_DE FROM METADATA_FULLTEXT");
            writer.deleteAll();
            // delete all indexes for creating new ones
            while (resultSet.next()) {
//i am done changes now lets run searcher
                String FTID = resultSet.getString("FTID"); // != null ?
                // resultSet.getString("FTID"):
                // " ";
                String ID = resultSet.getString("ID") != null ? resultSet.getString("ID") : " ";

                String CLASSID = resultSet.getString("CLASSID") != null ? resultSet.getString("CLASSID") : " ";

                String TEXT = resultSet.getString("TEXT") != null ? resultSet.getString("TEXT") : " ";

                String PUBNOTICECONTENT = resultSet.getString("PUBNOTICECONTENT") != null
                        ? resultSet.getString("PUBNOTICECONTENT") : " ";
                String DOCUMENTCONTENT = resultSet.getString("DOCUMENTCONTENT") != null
                        ? resultSet.getString("DOCUMENTCONTENT") : " ";
                String contentSum_DE = resultSet.getString("contentSum_DE") != null
                        ? resultSet.getString("contentSum_DE") : " ";

                Document doc = new Document();

                doc.add(new StringField("FTIDS", FTID, Field.Store.YES));
                doc.add(new StringField("IDS", ID, Field.Store.YES));
                doc.add(new StringField("CLASSIDS", CLASSID, Field.Store.YES));
                doc.add(new StringField("TEXTS", TEXT, Field.Store.YES));
                doc.add(new StringField("PUBNOTICECONTENTS", PUBNOTICECONTENT, Field.Store.YES));
                doc.add(new Field("DOCUMENTCONTENTS", DOCUMENTCONTENT, Field.Store.YES, Field.Index.ANALYZED));
                doc.add(new Field("contentSum_DES", contentSum_DE, Field.Store.YES, Field.Index.ANALYZED));

                if (writer.getConfig().getOpenMode() == OpenMode.CREATE) {
                    // New index, so we just add the document (no old document
                    // can be there):
                    System.out.println("adding " + FTID + " ---- " + ID + "---- " + CLASSID);
                    writer.addDocument(doc);
                }
            }

        } catch (Exception e) {
            System.out.println(e);
        }finally {
            if(resultSet != null)
                resultSet.close();
            if(statement != null)
                statement.close();
            if(connect != null)
                connect.close();
        }

    }

    // å°†querylogä¸­çš„æ•°æ�®æ›´æ–°åˆ°æ•°æ�®åº“ä¸­
    public static void savaToQueryLogDatabase(HashMap<String, QueryLog> queryLog) {

        Connection connect = null;
        Statement st = null;
        try {
            Class.forName(SQLModel.name);
            connect = DriverManager.getConnection(SQLModel.url, SQLModel.user, SQLModel.password);
            for(QueryLog eachQueryLog: queryLog.values()) {

                String doc_id = "" + eachQueryLog.doc_id;
                int doc_count = eachQueryLog.count;
                long totalTime = eachQueryLog.time;
                String queryString = eachQueryLog.queryString;
                /*
                String key = String.format("INSERT INTO QUERY_LOG(doc_id, query_string, doc_count, totaltime) " +
                            "VALUES(%d, '%s', 1, %d) ON DUPLICATE KEY UPDATE doc_count = %d, totaltime = %d",
                            doc_id, read, totalTime, doc_count ,totalTime);
                */

                String key = String.format("IF EXISTS (SELECT * FROM QUERY_LOG WHERE Doc_ID = '%s' and query_String = '%s') "
                                + " UPDATE QUERY_LOG SET Doc_Count= %d, totaltime= %d"
                                + " WHERE Doc_ID ='%s' ELSE INSERT INTO QUERY_LOG VALUES ('%s', '%s', %d, %d)",
                        doc_id, queryString, doc_count, totalTime, doc_id, doc_id, queryString, doc_count, totalTime);

                st = connect.createStatement();
                st.execute(key);
                LogUtils.log("Write each QueryLog back to Database\n", LogUtils.LOG_LEVEL.DEBUG);
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            try {
                connect.close();
            } catch (Exception e) {
                System.out.println(e);
            }
        }
    }

    public static void main(String[] args) {
        String connectionString =  "jdbc:sqlserver://114.215.174.91:1433;"
                + "databaseName=rank";

        Connection connection = null;



        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
            connection = DriverManager.getConnection(connectionString, "robert", "boss");
            //conn.setAutoCommit(true);

            PreparedStatement pst =  connection.prepareStatement("SELECT * FROM test");
            //pst.execute("INSERT INTO rank.dbo.test VALUES(1) ");

            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                System.out.print(rs.getInt(1));
            }

            pst.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        /*
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            connection = DriverManager.getConnection(connectionString);


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) try { connection.close(); } catch(Exception e) {}
        }*/
    }
}
