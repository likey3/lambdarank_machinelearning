package Utils;

/**
 * Created by andy on 16/5/19.
 */
public class LogUtils {
    public enum LOG_LEVEL {
        DEBUG, LIBOUTPUT, RELEASE
    }

    public static void log(String info, LOG_LEVEL level) {
        if (level == LOG_LEVEL.RELEASE || level == LOG_LEVEL.DEBUG)
            System.out.println(info);
    }
}
