package kmeans;

import org.apache.commons.math3.ml.clustering.CentroidCluster;
import org.apache.commons.math3.ml.clustering.KMeansPlusPlusClusterer;

import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * Created by liuchuliang on 16/5/19.
 */
public class KMeans {
    public static void main(String[] args) {
        List<Point> points = generatePoint(1);
        List<LocationWrapper> wrappers = new ArrayList<>(points.size());

        int i = 0;
        for (Point point : points) {
            wrappers.add(new LocationWrapper(point.x, point.y, i++));
        }

        getSortedLabel(wrappers);
    }

    public static int[] getSortedLabel(List<LocationWrapper> list) {
        int clusterNum = 3;

        if (list.size() <= 0) return new int[0];
        if (list.size() < clusterNum) clusterNum = list.size();

        KMeansPlusPlusClusterer<LocationWrapper> clusterer = new KMeansPlusPlusClusterer<LocationWrapper>(clusterNum, 1000);
        List<CentroidCluster<LocationWrapper>> result = clusterer.cluster(list);
        //List<Integer> sortedLabel = new ArrayList<>(list.size());
        int[] sortedLabel = new int[list.size()];

        Collections.sort(result, Comparator.comparing(x -> {
            double count = x.getCenter().getPoint()[0];
            double time = x.getCenter().getPoint()[1];

            return count + time;
        }));

        int rankLabel = 1;

        for (int i = 0; i < result.size(); i++) {
            System.out.println("label:" + i);

            for (LocationWrapper locationWrapper : result.get(i).getPoints()) {
                //sortedLabel.add(rankLabel);
                System.out.println(locationWrapper);
                sortedLabel[locationWrapper.index] = rankLabel;
            }

            if (result.get(i).getPoints().size() != 0) rankLabel++;

            System.out.println();
        }

        return sortedLabel;
    }

    public static List<Point> generatePoint(int n) {
        java.util.List<Point> result = new ArrayList<>(n);
        Random random = new Random(System.currentTimeMillis());

        for (int i = 0; i < n; i++) {
            Point p = new Point(random.nextInt(20),
                    //(int)(random.nextDouble() * 10000)
                    random.nextInt(20));

            result.add(p);
        }
        return result;
    }
}
