package kmeans;

import org.apache.commons.math3.ml.clustering.Clusterable;

/**
 * Created by liuchuliang on 16/5/19.
 */
public class LocationWrapper implements Clusterable{
    double count, time;
    int index;

    public LocationWrapper(int count, int time, int index) {
        this.count = 1.0 * count;
        this.time = 1.0 * time / 1000;
        this.index = index;
    }

    @Override
    public double[] getPoint() {
        return new double[]{count, time};
    }

    public int getIndex() {
        return index;
    }

    @Override
    public String toString() {
        return String.format("%g %g", count, time);
    }

}
